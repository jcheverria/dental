<?php

/**
 * File AuthController.php
 *
 * @author Tuan Duong <bacduong@gmail.com>
 * @package Laravue
 * @version 1.0
 */

namespace App\Http\Controllers;

use App\Laravue\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\UserResource;
use App\Laravue\Models\Role;
use App\Laravue\Models\User;
use App\Paciente;
use Illuminate\Support\Facades\Hash;

/**
 * Class AuthController
 *
 * @package App\Http\Controllers
 */
class AuthController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json(new JsonResponse([], 'login_error'), Response::HTTP_UNAUTHORIZED);
        }

        $user = $request->user();
        $token = $user->createToken('laravue');

        return response()->json(new UserResource($user), Response::HTTP_OK)->header('Authorization', $token->plainTextToken);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        try {
            $antecedentes = '';
            foreach ($request->antecendentes as $key => $value) {
                $antecedentes .= $value . ', ';
            }
            $params = $request->all();
            $user = User::create([
                'name' => $params['cedula'],
                'email' => $params['cedula'],
                'password' => Hash::make($params['password1']),
            ]);
            $role = Role::findByName('admin');
            $user->syncRoles($role);
            //tabla pacientes
            $paciente = Paciente::create([
                'primer_nombre' => $params['pnombre'],
                'segundo_nombre' => $params['snombre'],
                'primer_apellido' => $params['papellido'],
                'segundo_apellido' => $params['sapellido'],
                'sexo' => $params['sexo'],
                'edad' => $params['edad'],
                'antecedentes' => $antecedentes,
                'enfermedades' => $params['enfermedades_pre_existentes'],
                'cedula' => $params['cedula'],
            ]);
            $token = $user->createToken('laravue');

            return response()->json(new UserResource($user), Response::HTTP_OK)->header('Authorization', $token->plainTextToken);
        } catch (\Throwable $th) {
            return response()->json(new JsonResponse([], 'El usuario ya existe'), Response::HTTP_UNAUTHORIZED);
        }
        return true;
        /* $credentials = $request->only('email', 'password');
        if (!Auth::attempt($credentials)) {
            return response()->json(new JsonResponse([], 'create_error'), Response::HTTP_UNAUTHORIZED);
        }

        $user = $request->user();
        $token = $user->createToken('laravue');

        return response()->json(new UserResource($user), Response::HTTP_OK)->header('Authorization', $token->plainTextToken); */
    }

    public function logout(Request $request)
    {
        $request->user()->tokens()->delete();
        return response()->json((new JsonResponse())->success([]), Response::HTTP_OK);
    }

    public function user()
    {
        return new UserResource(Auth::user());
    }
}
