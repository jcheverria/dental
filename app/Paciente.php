<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paciente extends Model
{
    //
    protected $table = 'pacientes';

    const CREATED_AT = 'creacion';

    const UPDATED_AT = 'actualizacion';

    protected $fillable = [
        'primer_nombre',
        'segundo_nombre',
        'primer_apellido',
        'segundo_apellido',
        'sexo',
        'edad',
        'antecedentes',
        'enfermedades',
        'cedula',
    ];

    protected $dates = [
        'creacion',
        'actualizacion',
    ];
}
